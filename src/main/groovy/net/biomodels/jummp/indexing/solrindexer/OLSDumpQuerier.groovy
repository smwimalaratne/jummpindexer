/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.biomodels.jummp.indexing.solrindexer

import groovy.sql.Sql
import net.biomodels.jummp.annotationstore.ResourceReference
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 *
 * @author raza
 */
class OLSDumpQuerier extends OLSBasedAnnoProcessor {
    def sql
    private static final Logger logger = LoggerFactory.getLogger(OLSDumpQuerier.class)
    
    
    public OLSDumpQuerier(String server,
                          String port,
                          String database,
                          String username,
                          String password) {
        sql = Sql.newInstance("jdbc:mysql://${server}:${port}/${database}", username,
                              password, "com.mysql.jdbc.Driver")
        initialiseDataTypes()
    }
    
    void getAncestors(ResourceReference reference) {
        // Ancestors get loaded with term information
    }
    
    void getSynonyms(ResourceReference reference) {
        // Synonyms get loaded with term information
    }
    
    private void addSynonyms(String termID, ResourceReference reference) {
        String query = "select synonym_value from term_synonym where term_pk='${termID}'"
            sql.eachRow(query) {
                reference.addSynonym(it.synonym_value)
        }
    }
    
    
    ResourceReference getTermInformation(ResourceReference reference) {
        String id = reference.getAccession()
        String collType = reference.getDatatype()
        if (!cached.containsKey(id) && !loadFromDatabaseIntoCache(id, collType)) {
            String query = "SELECT term.* FROM term, ontology where shortName='${dictionary.get(collType)}' and identifier='${id}' and term.ontology_id = ontology.ontology_id"
            def row = sql.firstRow(query)
            reference.setName(row.term_name)
            reference.setDescription(row.definition)
            String termID = row.term_pk
            addSynonyms(termID, reference)
            addParents(termID, reference)
            saveResource(reference)
        }
        else {
            reference = cached.get(id)
        }
        return reference
    }
    
    private void saveResource(ResourceReference reference) {
        try {
            reference.save(failOnError:true)
            cached.put(reference.getAccession(), reference)
        }
        catch(Exception e) {
            e.printStackTrace()
           // System.exit(0)
            logger.error("Error writing $reference to db", e)
        }
    }
    
    private void addParents(String termID, ResourceReference ref) {
        Map<String, ResourceReference> processQueue = new HashMap<>()
        processQueue.put(termID, ref)
        String collType = ref.getDatatype()
        while (!processQueue.isEmpty()) {
            def nextQueue = [:]
            processQueue.each { termIdentifier, resource ->
                String query = "SELECT term.* FROM term, term_relationship where subject_term_pk='${termIdentifier}' and object_term_pk = term_pk"
                sql.eachRow(query) {
                    String parentID = it.identifier
                    if (!cached.containsKey(parentID) && !loadFromDatabaseIntoCache(parentID, collType)) {
                        ResourceReference parent = new ResourceReference(accession: parentID,
                                                                         datatype: collType,
                                                                         uri: "http://identifiers.org/${collType}/${parentID}",
                                                                         name: it.term_name,
                                                                         description: it.definition)
                        String parentTermID = it.term_pk
                        addSynonyms(parentTermID, parent)
                        nextQueue.put(parentTermID, parent)
                        saveResource(parent)
                    }
                    resource.addParent(cached.get(parentID))
                }
                saveResource(resource)
            }
            processQueue.clear()
            processQueue.putAll(nextQueue)
        }
    }
    
    void initialiseDataTypes() {
        if (sql) {
            sql.eachRow( 'select shortName from ontology' ) { 
                supportedTypes.add(it.shortName) 
            }
        }
    }
    
    
    public void shutdown() {
    }
}

