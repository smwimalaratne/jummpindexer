/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.biomodels.jummp.indexing.solrindexer

import java.util.concurrent.ConcurrentHashMap
import net.biomodels.jummp.annotationstore.ResourceReference
/**
 *
 * @author raza
 */
abstract class OLSBasedAnnoProcessor implements AncestryProvider, SynonymProvider, TermInformationProvider  {
    ConcurrentHashMap<String, ResourceReference> cached = [:]
    static def dictionary = ["go": "GO",
                             "bto": "BTO",
                             "cco": "CCO",
                             "cl": "CL",
                             "eco": "ECO",
                             "efo": "EFO",
                             "fma": "FMA",
                             "doid": "DOID",
                             "psimi": "MI",
                             "pw": "PW",
                             "pato": "PATO",
                             "psimod": "MOD",
                             "pr": "PRO",
                             "so": "SO",
                             "chebi" : "CHEBI"
                            ]

    Set<String> supportedTypes = new HashSet<>()
    
    public OLSBasedAnnoProcessor() {
        initialiseDataTypes()
    }
    
    boolean supportsAncestry() {
        return true
    }
    
    boolean supportsSynonyms() {
        return true
    }
    
    protected boolean loadFromDatabaseIntoCache(String id, String type) {
        ResourceReference ref = ResourceReference.findByAccessionAndDatatype(id, type)
        if (ref) {
            cached.put(id, ref)
            return true
        }
        return false
    }
    
    boolean supportsCollection(String collection) {
        return supportedTypes.contains(dictionary.get(collection))
    }

    abstract protected void initialiseDataTypes()
    
}

