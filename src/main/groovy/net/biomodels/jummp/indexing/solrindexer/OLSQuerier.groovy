/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.biomodels.jummp.indexing.solrindexer

import java.rmi.RemoteException
import java.util.concurrent.Callable
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Phaser
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import uk.ac.ebi.ontocat.OntologyService
import net.biomodels.jummp.indexing.solrindexer.ols.Query
import net.biomodels.jummp.indexing.solrindexer.ols.QueryServiceLocator
import uk.ac.ebi.ontocat.OntologyServiceException
import uk.ac.ebi.ontocat.OntologyTerm
import uk.ac.ebi.ontocat.ols.OlsOntologyService
import net.biomodels.jummp.annotationstore.ResourceReference
/**
 *
 * @author raza
 */
class OLSQuerier extends OLSBasedAnnoProcessor {

    private static final Logger logger = LoggerFactory.getLogger(OLSQuerier.class)
    private OntologyService os = new OlsOntologyService()
    final int POOL_SIZE = computePoolSize()
    ExecutorService pool = Executors.newFixedThreadPool(POOL_SIZE)


    public OLSQuerier() {
        initialiseDataTypes()
    }

    public void shutdown() {
        pool.shutdown()
    }

    void getAncestors(ResourceReference reference) {
        // Ancestors get loaded with term information
    }

    void getSynonyms(ResourceReference reference) {
        // Synonyms get loaded with term information
    }

    ResourceReference getTermInformation(ResourceReference reference) {
        return retrieveTermInfo(reference)
    }

    void initialiseDataTypes() {
        try {
            HashMap<String, String> ontologies = getQuery().getOntologyNames();
            if (ontologies){
                supportedTypes.addAll(ontologies.keySet());
            }
        } catch (RemoteException e) {
            logger.debug(e.getMessage(), e);
        }
    }

    private Query getQuery() {
        QueryServiceLocator locator = new QueryServiceLocator()
        return locator.getOntologyQuery()
    }

    private ResourceReference retrieveTermInfo(ResourceReference res) {
        String id = res.getAccession()
        String collType = res.getDatatype()
        String type = dictionary.get(collType)
        if (!cached.containsKey(id) && !loadFromDatabaseIntoCache(id, collType)) {
            try {
            Phaser phaser = new Phaser()
            phaser.register()
            Map<OntologyTerm, ResourceReference> queue = new ConcurrentHashMap<>()
            ConcurrentHashMap<String, Vector> parentage = new ConcurrentHashMap<>()
            queue.put(os.getTerm(type, id), res)
            while (!queue.isEmpty()) {
                logger.info(queue.toString())
                long start = System.currentTimeMillis()
                logger.debug("${queue.size()}")
                Map<OntologyTerm> queueCopy = new HashMap<>(queue.size())
                queueCopy.putAll(queue)
                queue.clear()
                queueCopy.each { OntologyTerm elem, ResourceReference resource ->
                    phaser.register()
                    def task = [call: {
                        try {
                            String accession = elem.getAccession()
                            if (!cached.containsKey(accession)) {
                                def synonyms = getSynonymsFromOLS(elem)
                                def parents = getParentsFromOLS(elem)

                                /*def data = getQuery().getTermMetadata(queryTerm, type)
                                  data.put("name", getQuery().getTermById(queryTerm, type))
                                  def parents = getQuery().getTermParents(queryTerm, type)*/
                                cached.put(accession,
                                    constructResource(resource, elem, synonyms))
                                if (!parents.isEmpty()) {
                                    parentage.put(accession, new Vector<String>())
                                }
                                parents.each {
                                    String parentID = it?.getAccession()
                                    //String parentID = it.key
                                    if (parentID && !cached.containsKey(parentID) && !loadFromDatabaseIntoCache(parentID, collType)) {
                                        ResourceReference reference = new ResourceReference(accession: parentID,
                                                            datatype: collType,
                                                            uri: "http://identifiers.org/${collType}/${parentID}");
                                        queue.put(it, reference)
                                        parentage.get(accession).add(parentID)
                                    }
                                }
                            }
                        }
                        catch(Exception e) {
                            logger.error("There was a problem querying OLS: ${e.message}", e)
                        }
                        finally {
                            phaser.arriveAndDeregister()
                        }
                    }] as Callable
                    pool.submit(task)
                }
                phaser.arriveAndAwaitAdvance()
                start = System.currentTimeMillis() - start;
                logger.info("${queueCopy.size()}, $start")
            }

            parentage.each { term, parents ->
                ResourceReference ref = cached.get(term)
                parents.each { parent ->
                    ref.addParent(cached.get(parent))
                }
                ref.save()
            }
            }
            catch(Exception e) {
                logger.error("Exception processing $id: ${e.message}", e)
            }
        }
        res = cached.get(id)
        return res
    }

    private def doInOLSWrapper(Closure action) {
        try {
            return action.call()
        }
        catch(OntologyServiceException olsError) {
            logger.debug("Error calling OLS ${olsError.message}", olsError)
            return []
        } catch(NullPointerException e) {
            logger.warn("Why did we get this ${e.message}", e)
            return []
        }
    }

    private List<String> getSynonymsFromOLS(OntologyTerm term) {
        def annotations = doInOLSWrapper {os.getAnnotations(term)}
        List<String> retval = new LinkedList<>()
        if (!annotations.isEmpty()) {
            annotations.each { key, value ->
                if (key.contains("synonym")) {
                        retval.addAll(value)
                }
            }
        }
        return retval
    }

    private List<OntologyTerm> getParentsFromOLS(OntologyTerm term) {
        return doInOLSWrapper {os.getParents(term)}
    }

    private ResourceReference constructResource(ResourceReference reference, OntologyTerm term, List<String> synonyms) {
        //reference.setDescription(term.getLabel())
        reference.setName(term.getLabel())
        synonyms.each {
            reference.addSynonym(it)
        }
        if (!reference.save()) {
            logger.error("ERROR SAVING ${reference}")
        }
        return reference
    }

    /**
     * Calculates the pool size based on the number of available processors.
     *
     * Internally, it uses the formula
     *      N_threads = N_cpu * U_cpu * (1 + W/C) where
     *      N_cpu = number of CPU cores or hardware threads
     *      U_cpu = target CPU utilisation - 90%
     *      W/C = ratio of wait over compute time - 85% waiting time.
     * @return the size of the pool as an integer.
     */
    private int computePoolSize() {
        final float TARGET_CPU_USAGE = 0.9f
        final int ESTIMATED_WAIT_TIME = 85
        final float WAIT_COMPUTE_RATIO = 1 + ESTIMATED_WAIT_TIME / (100 - ESTIMATED_WAIT_TIME)
        int result = (int) (2 * Runtime.getRuntime().availableProcessors() *
                TARGET_CPU_USAGE * WAIT_COMPUTE_RATIO)
        logger.info("Setting thread pool size to $result")
        result
    }

}

