/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer
import net.biomodels.jummp.annotationstore.ResourceReference
/**
 * Interface for retrieving information about an annotation term found in a model.
 *
 * A term represents either the subject or the object of an RDF statement, and
 * may be a URI to a term in an ontology or controlled vocabulary, or just plain text.
 *
 * Annotation resolvers can use different implementations of this interface to extract
 * information about a term based on the provenance of the term, for instance ChEBI or GO.
 *
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 */
interface TermInformationProvider {
    /**
     * Used by AnnotationResolver to ask if a term information provider supports
     * the given @p collection
    **/
    boolean supportsCollection(String collection)
    /**
     * Populates the supplied @p reference with information from external sources.
     * @param reference the annotation term for which to fetch information.
     * @returns a resource reference object (potentially a different object, 
     * depending on whether info is retreieved from DB
     */
    ResourceReference getTermInformation(ResourceReference reference)
}
