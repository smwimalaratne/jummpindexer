/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import com.hp.hpl.jena.rdf.model.Literal
import com.hp.hpl.jena.rdf.model.Model
import com.hp.hpl.jena.rdf.model.Statement
import eu.ddmore.metadata.service.MetadataValidator
import eu.ddmore.metadata.service.MetadataValidatorImpl
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationContext
import org.springframework.context.support.ClassPathXmlApplicationContext

import javax.xml.stream.XMLStreamReader
import net.biomodels.jummp.annotationstore.Qualifier
import net.biomodels.jummp.annotationstore.ResourceReference
/**
 *
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @author raza
 * @author Sarala Wimalaratne
 */
class PharmlIndexer extends ModelIndexer {
    private static final Logger log = LoggerFactory.getLogger(this)
    private static final boolean IS_INFO_ENABLED = log.isInfoEnabled()

    public void extractFileContent(Map indexData, String modelFilePath, List<String> allFiles) {
        final File pharmML = new File(modelFilePath)
        final List<String> descriptions = extractElementDescriptions(pharmML)
        if (descriptions) {
            indexData.put("elementDescription", descriptions)
        }
        String metadataFileName = JummpXmlUtils.findModelAttribute(pharmML, "PharmML",
                "metadataFile") ?: allFiles.find { it.endsWith('.rdf') }
        if (metadataFileName) {
            if (IS_INFO_ENABLED) {
                log.info("Found annotations for $modelFilePath in $metadataFileName")
            }
            final String rdfFileLocation = allFiles.find {
                it.endsWith(metadataFileName) &&
                        JummpXmlUtils.containsElement(new File(it), "RDF")
            }
            if (rdfFileLocation) {
                Map annotationsMap = parseAnnotations(rdfFileLocation, pharmML, indexData)
                if (IS_INFO_ENABLED) {
                    log.info("Finished indexing ${annotationsMap.size()} annotations for $modelFilePath")
                }
            } else {
                log.error "Cannot find anno file $metadataFileName for model $modelFilePath"
            }
        }
    }

    private List<String> extractElementDescriptions(File pharmML) {
        final List<String> result = []
        if (pharmML && pharmML.canRead()) {
            JummpXmlUtils.parseXmlFile.curry(pharmML) { XMLStreamReader r ->
                if ("Description".equalsIgnoreCase(r.getLocalName())) {
                    result.add r.getElementText()
                }
                return false
            }
        } else {
            log.warn "Cannot extract element descriptions from file $pharmML"
        }
        return result
    }

    private Map parseAnnotations(String annotationFileLocation, File mainPharmML, Map indexData) {
        def annotations = [:]
        if (!annotationFileLocation) {
            log.warn "Cannot parse annotations for undefined annotation file."
            return annotations
        }
        if (IS_INFO_ENABLED) {
            log.info "Parsing annotations defined in $annotationFileLocation"
        }
        final File rdfFile = new File(annotationFileLocation)
        if (!rdfFile.canRead()) {
            log.warn "Cannot access annotations contained in $annotationFileLocation"
            return annotations
        }

        def contentMappings = [
            "model-has-publication-source-quoted": "pharmmlPublicationSourceText",
            "model-has-description-short": "description",
            "model-has-description-long": "pharmmlLongTechnicalDescription",
            "model-has-description": "pharmmlModellingContext",
            "model-has-author": "modelAuthors",
            "model-has-indication": "pharmmlModelIndication",
            "model-has-rationale": "pharmmlModelRationale",
            "model-has-population-characteristic": "pharmmlPopulation",
            "elt-hasint": "pharmmlElementInterpretation",
            "elt-hasunit": "pharmmlElementUnit",
            "model-has-modelling-limitations-textual": "pharmmlLimitations",
            "elt-hassymbid": "pharmmlSymbolId",
            "model-has-publication-source-external-id": "pharmmlPublicationURI",
            "model-type-of-data": "pharmmlTypeOfData",
            "model-research-stage": "pharmmlResearchStage",
            "has-interpreted-type": "pharmmlInterpretedType",
            "model-tasks-in-scope": "pharmmlTasks",
            "model-study-type" : "pharmmlStudyType",
            "model-field-purpose": "pharmmlModelFieldPurpose",
            "model-modelling-question": "pharmmlModellingQuestion",
            "model-has-name": "name",
            "model-related-to-drug" : "pharmmlModelDrug",
            "elt-hasunit": "pharmmlElementUnit",
            //"type": "pharmmlElementType",
            "model-sampling-type": "pharmmlSamplingType",
            "model-type-of-data": "pharmmlTypeOfData",
            "has-therapeutic-area" : "pharmmlTherapeuticArea",
            "has-generic-type" : "pharmmlGenericType",
            "model-clinical-context" : "pharmmlClinicalContext",
            "model-phenomenal-purpose" : "pharmmlPhenomenalPurpose"
        ]

        ApplicationContext context = new ClassPathXmlApplicationContext("metadatalib-spring-config.xml");
        MetadataValidator reader = context.getBean(MetadataValidatorImpl.class);
      //  MetadataValidator reader = new MetadataValidatorImpl()
        Model annoModel = reader.read(rdfFile)

        def iterator = annoModel.listStatements()
        while (iterator.hasNext()) {
            Statement stmt = iterator.nextStatement()
            def s = stmt.subject
            def p = stmt.predicate
            String pName = p.localName
            String pURI = p.getURI()
            def o = stmt.object
            if (contentMappings.containsKey(pName)) {
                def solrFieldName = contentMappings[pName]
                def content = o.literal ?
                    ((Literal) o).getString() :
                    ((com.hp.hpl.jena.rdf.model.Resource) o).getURI()
                if (annotations.containsKey(solrFieldName)) {
                    //append to existing values
                    def existing = annotations[solrFieldName]
                    if (existing instanceof Collection) {
                        existing.addAll content
                    } else if (existing instanceof CharSequence) {
                        def newContent = [existing]
                        newContent.addAll(content)
                        annotations.put(solrFieldName, newContent)
                    }
                } else { // new field
                    annotations.put(solrFieldName, content)
                }
                if (content) {
                    Qualifier qual = Qualifier.findByUri(pURI)
                    if (!qual) {
                        qual = new Qualifier(accession:pName, uri: pURI,
                            qualifierType: 'pharmML')
                        if (!qual.save()) {
                            log.error("""\
Unable to save qualifier $pURI in db while indexing $annotations:${qual.errors.allErrors.dump()}
""")
                        }
                    }
                    indexData.putAll(annotations)
                    indexAncestors(content, indexData, qual, s.getURI())
                }
            }
        }
        return annotations
    }

    @Override
    protected void indexAncestors(String uriOrLabel, Map indexData, Qualifier qualifier,
            String subjectId) {
        def annoType = AnnotationReferenceResolver.instance().getType(uriOrLabel)
        if (annoType != AnnotationReferenceResolver.AnnoType.UNKNOWN) {
            super.indexAncestors(uriOrLabel, indexData, qualifier, subjectId)
        } else {
            if (IS_INFO_ENABLED) {
                log.info("Indexing ancestors for $uriOrLabel")
            }
            ResourceReference reference = ResourceReference.findByUri(uriOrLabel)
            if (reference) {
                def queue = [reference]
                if (reference.parents) {
                    queue.addAll(reference.parents)
                }
                queue.each { ResourceReference r ->
                    if (r.getAccession()) {
                        if (indexData.containsKey("externalReferenceID")) {
                            indexData.externalReferenceID.add(r.getAccession())
                        } else {
                            indexData.externalReferenceID = [r.getAccession()]
                        }
                    }
                    if (r.getName()) {
                        if (indexData.containsKey("externalReferenceName")) {
                            indexData.externalReferenceName.add(r.getName())
                        } else {
                            indexData.externalReferenceName = [r.getName()]
                        }
                    }
                }
            } else {
                reference = new ResourceReference(datatype: "unknown")
                if (uriOrLabel.startsWith("http:")) {
                    log.warn "Found no references for uri $uriOrLabel"
                    reference.uri = uriOrLabel
                } else {
                    reference.name = uriOrLabel
                }
                if (!reference.save()) {
                    log.error("resource $uriOrLabel not saved: ${reference.errors.allErrors.dump()}")
                    return
                }
            }
            saveAnnoToDB(reference, qualifier, subjectId, indexData.revision_id)
        }
    }
}

