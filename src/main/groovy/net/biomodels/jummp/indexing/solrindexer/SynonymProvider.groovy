/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer
import net.biomodels.jummp.annotationstore.ResourceReference
/**
 * Interface for retrieving synonyms for an annotation term found in a model.
 *
 * Term synonyms represent the alternative names used by subject experts to refer
 * to the concept which is mapped by an annotation term.
 *
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 */
interface SynonymProvider {
    /**
     * Indicates whether an implementation can provide synonyms
     * @return true if synonyms are extracted, false otherwise.
     */
    boolean supportsSynonyms()
    /**
     * Retrieves the synonyms of a given annotation term.
     * @param reference the annotation term for which to extract synonyms.
     */
    void getSynonyms(ResourceReference reference)
}
