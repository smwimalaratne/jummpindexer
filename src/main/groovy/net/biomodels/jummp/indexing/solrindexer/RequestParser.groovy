/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 *
 * @author raza
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 */
class RequestParser {
    private static final Logger log = LoggerFactory.getLogger(RequestParser.class)

    public static void handleRequest(String jsonPath) {
        RequestContext ctx = new RequestContext(jsonPath)
        if (!ctx.isEmpty()) {
            String cfgPath = ctx.configFilePath
            GormUtil.initGorm(ctx)
            AnnotationReferenceResolver.initialiseWithProperties(cfgPath)
            ModelIndexer indexer
            switch (ctx.modelFormat) {
                case "SBML":
                    indexer = new SBMLIndexer()
                    break
                case "PharmML":
                    indexer = new PharmlIndexer()
                    break
                default:
                    indexer = new GenericModelIndexer()
            }
            Map data = ctx.partialData
            try {
                String mainFilePath = ctx.mainFilePaths.first()
                List<String> allFilePaths = ctx.allFilePaths
                indexer.extractFileContent(data, mainFilePath, allFilePaths)
            }
            catch(Exception e) {
                String docPath = ctx.indexFile.absolutePath
                log.error("Exception indexing ${docPath} - ${e.message}", e)
            }
            AnnotationReferenceResolver.instance().shutdown()
            String solrUrl = ctx.solrServer
            indexer.indexData(data, solrUrl)
        }
    }
}
