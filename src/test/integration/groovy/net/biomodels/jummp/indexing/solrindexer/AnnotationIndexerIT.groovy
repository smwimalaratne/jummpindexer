/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import groovy.json.JsonSlurper
import net.biomodels.jummp.model.*
import net.biomodels.jummp.plugins.security.Person
import net.biomodels.jummp.plugins.security.User
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

import static org.junit.Assert.*

/**
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 */
@RunWith(JUnit4.class)
class AnnotationIndexerIT {
    private static final Logger logger = LoggerFactory.getLogger(AnnotationIndexerIT.class)

    @Test
    void testIndexingAnnotations() {
        File f = new File("src/test/resources/models/sbml-anno/indexData.json")
        assertTrue f.exists()
        RequestContext ctx = new RequestContext(f.absolutePath)
        ctx.configFilePath =
            "${System.getProperty('user.home')}${File.separatorChar}.jummp.properties"
        String cfgPath = ctx.configFilePath
        GormUtil.isTestEnvironment = true
        GormUtil.initGorm(ctx)

        Person p = new Person(userRealName: 'not me', institution: "M.E. PLC")
        assertNotNull(p.save())
        assertFalse(p.hasErrors())
        User self = new User(username: 'me', email: 'my@self.name', person: p,
                passwordExpired: false, password: 'obscure', accountExpired: false,
                accountLocked: false, enabled: true)
        assertNotNull(self.save())
        assertFalse(self.hasErrors())
        Model model = new Model(vcsIdentifier: "test/", submissionId: "MODEL00000400")
        def sbmlL2V4 = new ModelFormat(name: "SBML", identifier: "SBML", formatVersion: "L2V4")
        sbmlL2V4.validate()
        assertNotNull(sbmlL2V4.save())
        assertFalse(sbmlL2V4.hasErrors())
        def revision = new Revision(model: model, vcsId: "1", revisionNumber: 1,
                owner: self, minorRevision: false, name:"interesting model v1",
                uploadDate: new Date(), format: sbmlL2V4)
        assertTrue(revision.validate())
        /*
         * Normally, we would now call
         *     model.addToRevisions(revision)
         * but until we upgrade to GORM v4, there is a problem that precludes
         * dynamic methods from being added to the metaClass of domain objects
         * in applications that only use GORM, rather than the full Grails stack.
         *
         * One workaround is to employ DefaultGroovyMethods.setProperty(), which is
         * what we've done here, another would be to use the meta class of
         * HibernateUtils.groovy to call gormEnhancer.enhance(entity, false), just like
         * the patch below does.
         *
         * @see https://github.com/grails/grails-data-mapping/commit/ddf8e181
         */
        model.revisions = [revision]
        assertNotNull(model.save(flush: true))
        assertFalse(model.hasErrors())
        assertNotNull(revision.save(flush: true))
        assertFalse(revision.hasErrors())

        AnnotationReferenceResolver.initialiseWithProperties(cfgPath)
        assertEquals("SBML", ctx.modelFormat)
        Map<String, List<String>> indexData = ctx.partialData
        SBMLIndexer indexer = new SBMLIndexer()
        def time = System.nanoTime()
        String modelPath = ctx.mainFilePaths.first()
        indexer.extractFileContent(indexData, modelPath, [])
        time = (System.nanoTime() - time) / 1000000.0
        logger.info "It took ${time}ms to index the model."
        assertTrue(indexData.size() > 0)
   }
}
