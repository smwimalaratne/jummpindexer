/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.apache.solr.client.solrj.SolrClient
import org.apache.solr.client.solrj.impl.HttpSolrClient
import org.apache.solr.common.SolrInputDocument
import net.biomodels.jummp.annotationstore.Statement
import net.biomodels.jummp.annotationstore.ElementAnnotation
import net.biomodels.jummp.annotationstore.Qualifier
import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.model.Revision
/**
 *
 * @author raza
 */
abstract class ModelIndexer {
    SolrClient solrClient

    private static final Logger log = LoggerFactory.getLogger(this.getClass())

    public abstract void extractFileContent(Map indexData, String modelFilePath, List<String> allFiles)

    public void indexData(Map data, String solrURL) {
        solrClient = new HttpSolrClient(solrURL)
        SolrInputDocument doc = new SolrInputDocument()
        data.each { entry, oneOrMoreValues ->
            if (oneOrMoreValues instanceof String) {
                if (entry == "submissionDate" || entry == "lastModified") {
                    def asDate = Date.parse("yyyy-MM-dd'T'HH:mm:ssZ", oneOrMoreValues)
                    doc.addField(entry, asDate)
                }
                else {
                    doc.addField(entry, oneOrMoreValues)
                }
            }
            else {
                oneOrMoreValues.each { singleValue ->
                    doc.addField(entry, singleValue)
                }
            }
        }
        solrClient.add(doc)
    }

    protected def getOrCreate(String key, Map indexData) {
        if (!indexData.containsKey(key)) {
            indexData.put(key, [])
        }
        return indexData.get(key)
    }

    protected void indexAncestors(String urn, Map indexData, Qualifier qualifier, String subjectId) {
        def annoType = AnnotationReferenceResolver.instance().getType(urn)
        Set<Integer> added = new HashSet<Integer>()
        ResourceReference reference = null
        if (annoType !=  AnnotationReferenceResolver.AnnoType.UNKNOWN) {
            String[] urnInfo = AnnotationReferenceResolver.instance().extractParts(urn, annoType)
                if (AnnotationReferenceResolver.instance().isSupported(urnInfo[0])) {
                    reference = AnnotationReferenceResolver
                                                    .instance()
                                                    .resolve(urnInfo[1], urnInfo[0])
                Set<ResourceReference> unprocessed = new HashSet<>()
                    unprocessed.add(reference)
                    while (!unprocessed.isEmpty()) {
                        Set<ResourceReference> nextIteration = new HashSet<>()
                        unprocessed.each {
                            getOrCreate("externalReferenceID", indexData).add(it.getAccession())
                            getOrCreate("externalReferenceName", indexData).add(it.getName())
                            getOrCreate("externalReferenceName", indexData).addAll(it.getSynonyms())
                            added.add(it.id)
                            it.getParents().each { parent ->
                                if (!added.contains(parent.id)) {
                                    nextIteration.add(parent)
                                }
                            }
                        }
                        unprocessed.clear()
                        unprocessed.addAll(nextIteration)
                    }
                }
                else {
                    reference = new ResourceReference(uri: urn, datatype: urnInfo[0])
                    if (!reference.save()) {
                        log.error("Could not save resource with uri ${urn}")
                        return
                    }
                }
        }
        else {
            reference = new ResourceReference(uri: urn, datatype: 'unknown')
            if (!reference.save()) {
                log.error("Could not save resource with uri ${urn}")
            }
        }
        saveAnnoToDB(reference, qualifier, subjectId, indexData.revision_id)
    }

    protected void saveAnnoToDB(ResourceReference reference, Qualifier qualifier, String subjectId, Long revisionId) {
        if (qualifier && subjectId && reference) {
            Statement statement = new Statement(subjectId: subjectId, qualifier: qualifier, object: reference)
            ElementAnnotation annotation = new ElementAnnotation(statement: statement, creatorId: 'raza', revision:
                    Revision.load(revisionId))
            if (!annotation.save()) {
                log.error "Cannot save annotation ${annotation.dump()}: ${annotation.errors.allErrors.dump()}"
            }
        }
    }
}

