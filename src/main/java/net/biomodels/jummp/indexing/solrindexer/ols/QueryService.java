/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.biomodels.jummp.indexing.solrindexer.ols;

/**
 *
 * @author raza
 */

public interface QueryService extends javax.xml.rpc.Service {
    public java.lang.String getOntologyQueryAddress();

    public Query getOntologyQuery() throws javax.xml.rpc.ServiceException;

    public Query getOntologyQuery(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}