/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.biomodels.jummp.indexing.solrindexer
import net.biomodels.jummp.annotationstore.ResourceReference
/**
 *
 * @author raza
 */
class AnnotationReference {
    AncestryProvider ancestryProvider;
    TermInformationProvider termInformationProvider;
    SynonymProvider synonymProvider;
    
    ResourceReference reference;
    
    public boolean hasAncestryProvider() {
        return ancestryProvider!=null
    }
    
    public boolean hasTermInformationProvider() {
        return termInformationProvider!=null
    }
    
    public boolean hasSynonymProvider() {
        return synonymProvider!=null
    }
    
    public ResourceReference getResource() {
        if (hasTermInformationProvider()) {
            reference=termInformationProvider.getTermInformation(reference)
        }
        if (hasAncestryProvider()) {
            ancestryProvider.getAncestors(reference)
        }
        if (hasSynonymProvider()) {
            synonymProvider.getSynonyms(reference)
        }
        return reference
    }
    
    public static AnnotationReference constructAnnotationReference(AncestryProvider ancestryProvider,
                                                                   TermInformationProvider termInformationProvider,
                                                                   SynonymProvider synonymProvider,
                                                                   String id,
                                                                   String type) {
        ResourceReference reference = new ResourceReference(accession: id,
                                                            datatype: type,
                                                            uri: "http://identifiers.org/${type}/${id}");
        AnnotationReference annoReference = new AnnotationReference(ancestryProvider: ancestryProvider,
                                                                    termInformationProvider: termInformationProvider,
                                                                    synonymProvider: synonymProvider,
                                                                    reference: reference
                                                                    )
        return annoReference
    }
}

