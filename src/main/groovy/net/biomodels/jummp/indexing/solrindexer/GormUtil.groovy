/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import grails.orm.bootstrap.HibernateDatastoreSpringInitializer
import groovy.transform.CompileStatic
import net.biomodels.jummp.annotationstore.*
import net.biomodels.jummp.model.*
import net.biomodels.jummp.plugins.security.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationContext
import javax.sql.DataSource
import com.mchange.v2.c3p0.ComboPooledDataSource

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author raza
 */
@CompileStatic
public class GormUtil {
    private static final Logger log = LoggerFactory.getLogger(GormUtil.class)
    private static final boolean IS_DEBUG_ENABLED = log.isDebugEnabled()
    private static boolean isTestEnvironment = false
    static ApplicationContext appCtx

    static void initGorm(RequestContext ctx) {
        if (IS_DEBUG_ENABLED) {
            log.debug "Started GORM bootstrapping..."
        }
        List classes = [Model,
                ModelFormat,
                Publication,
                PublicationLinkProvider,
                RepositoryFile,
                Revision,
                Qualifier,
                Statement,
                ElementAnnotation,
                ResourceReference,
                Role,
                Person,
                User,
                UserRole
        ]
        Properties props = [
            "hibernate.hbm2ddl.auto": isTestEnvironment ? "create-drop" : "update",
            /* C3P0 connection pool settings  */
            "hibernate.c3p0.min_size": "5",
            "hibernate.c3p0.max_size": "20",
            "hibernate.c3p0.timeout": "1800",
            "hibernate.c3p0.max_statements": "50"
        ] as Properties
        HibernateDatastoreSpringInitializer init =
                new HibernateDatastoreSpringInitializer(props, classes)
        DataSource dataSource = createDataSource(ctx)
        appCtx = init.configureForDataSource(dataSource)
        if (IS_DEBUG_ENABLED) {
            log.debug "...finished bootstrapping GORM."
        }
    }

    private static DataSource createDataSource(RequestContext ctx) {
        String url = ctx.databaseURL
        String username = ctx.databaseUsername
        String pwd = ctx.databasePassword
        boolean haveDatabaseDetails = url && username

        if (!haveDatabaseDetails) {
            final String path = ctx.configFilePath
            Properties jummpProps = new Properties()
            Reader reader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(path), "UTF-8"))
            try {
                jummpProps.load(reader)
            } finally {
                reader?.close()
            }
            String dbType = jummpProps.getProperty("jummp.database.type")
            String protocol = dbType.toLowerCase()
            String server = jummpProps.getProperty("jummp.database.server")
            String port = jummpProps.getProperty("jummp.database.port")
            String dbName = jummpProps.getProperty("jummp.database.database")
            url = "jdbc:$protocol://$server:$port/$dbName".toString()
            username = jummpProps.getProperty("jummp.database.username")
            pwd = jummpProps.getProperty("jummp.database.password")
        }

        def ds = new ComboPooledDataSource()
        ds.jdbcUrl = url
        ds.user = username
        ds.password = pwd
        ds
    }
}
