/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.sbml.jsbml.Model
import org.sbml.jsbml.SBMLDocument
import org.sbml.jsbml.SBMLReader
import org.sbml.jsbml.ext.fbc.FBCModelPlugin
import org.sbml.jsbml.ext.fbc.FluxBound

import static org.junit.Assert.*

/**
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 */
@RunWith(JUnit4.class)
class SBMLIndexerTest {
    @Test
    public void testFbcIndexing() {
        File modelFile = new File("src/test/resources/models/sbml-fbc/fbc_example1.xml")
        assertTrue(modelFile.exists())
        def is = this.getClass().classLoader.getResourceAsStream(
                "models/sbml-fbc/fbc_example1.xml")
        assertNotNull(is)
        SBMLDocument d = new SBMLReader().readSBMLFromStream(is)
        assertNotNull(d)
        Model m = d.getModel()
        assertNotNull(m)
        assertTrue(m.isSetPlugin("fbc"))
        FBCModelPlugin plugin = (FBCModelPlugin) m.getPlugin("fbc")
        def fluxBounds = plugin.getListOfFluxBounds()
        assertEquals(1, fluxBounds.size())
        FluxBound fb = fluxBounds.first()
        assertEquals("bound1", fb.getId())
        assertEquals("J0", fb.getReaction())
        assertEquals("EQUAL", fb.getOperation().name())
        assertTrue(10 == fb.getValue())

        SBMLIndexer indexer = new SBMLIndexer()
        def indexData = [:]
        indexer.extractFileContent(indexData, modelFile.absolutePath, [])
        def names = indexData.elementName
        assertEquals(['Sample FBC model'] as HashSet<String>, names)
        assertEquals([] as HashSet<String>, indexData.elementDescription)
        assertEquals([] as HashSet<String>, indexData.externalReferenceID)
        assertEquals([] as HashSet<String>, indexData.externalReferenceName)
        assertEquals([] as HashSet<String>, indexData.modelAuthors)
        assertEquals([] as HashSet<String>, indexData.vCardModelAuthors)
        HashSet<String> actualIDs = indexData.elementID
        def species = [*0..9].collect { "Node$it".toString() }
        def reactions = [*0..10].collect { "J$it".toString() }
        HashSet<String> expectedIDs = new HashSet<>()
        expectedIDs.addAll(['Model_1', 'compartment', 'bound1', 'obj1'])
        expectedIDs.addAll species
        expectedIDs.addAll reactions
        /*
         * Since actualIDs is a HashSet, we cannot make any assumption about the order of the
         * elements. Two sets A, B are identical if all elements in A are in B are vice-versa.
         */
        expectedIDs.each { assertTrue(actualIDs.contains(it)) }
        actualIDs.each { assertTrue(expectedIDs.contains(it)) }
    }
}
